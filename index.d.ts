declare module "avanza" {
    export interface Credentials {
        username: string,
        password: string,
        totp?: string,
        totpSecret?: string
    }
    export interface Position {
        accountId: string
        accountName: string
        accountType: string
        acquiredValue: number
        averageAcquiredPrice: number
        change: number
        changePercent: number
        currency: string
        depositable: boolean
        flagCode: string
        lastPrice: number
        lastPriceUpdated: string
        name: string
        orderbookId: string
        profit: number
        profitPercent: number
        tradable: boolean
        value: number
        volume: number
    }
    export interface InstrumentTypePosition {
        instrumentType: string
        positions: Array<Position>
        todaysProfitPercent: number
        totalProfitPercent: number
        totalProfitValue: number
        totalValue: number
    }
    export interface PositionSummary {
        instrumentPositions: Array<InstrumentTypePosition>
        totalBalance: number
        totalBuyingPower: number
        totalOwnCapital: number
        totalProfit: number
        totalProfitPercent: number
    }
    export interface OrderOptions {
        accountId: string,
        orderBookId: string | number
        orderType: "BUY" | "SELL"
        price: number
        validUntil: string
        volume: number
    }
    export interface OrderResponse {
        messages: any,
        requestId: any,
        status: any
        orderId: any
    }
    export interface OverviewAccount {
        accountId: string
        accountPartlyOwned: boolean
        accountType: string
        active: boolean
        attorney: boolean
        buyingPower: number
        depositable: boolean
        interestRate: number
        name: string
        ownCapital: number
        performance: number
        performancePercent: number
        totalBalance: number
        totalBalanceDue: number
        totalProfit: number
        totalProfitPercent: number
        tradable: boolean
    }
    export interface Overview {
        accounts: Array<OverviewAccount>
        numberOfDeals: number
        numberOfIntradayTransfers: number
        numberOfOrders: number
        numberOfTransfers: number
        totalBalance: number
        totalBuyingPower: number
        totalOwnCapital: number
        totalPerformance: number
        totalPerformancePercent: number
    }
    export interface Orderbook {
        change: number
        changePercent: number
        currency: string
        flagCode: string
        highestPrice: number
        id: string
        instrumentType: string
        lastPrice: number
        lowestPrice: number
        name: string
        priceThreeMonthsAgo: number
        totalVolumeTraded: number
        tradable: boolean
        updated: string
    }
    export default class Avanza {
        readonly _authenticated: boolean;
        readonly _authenticationTimeout: number
        authenticate: (credentials: Credentials) => Promise<void>
        getOrderbooks: (orderbookIds: Array<number>) => Promise<Orderbook[]>
        placeOrder: (options: OrderOptions) => Promise<OrderResponse>
        getPositions: () => Promise<PositionSummary>
        getOverview: () => Promise<Overview>
        getInstrument: () => Promise<void>
        disconnect: () => void;
        static STOCK: string;
        static FUND: string;
        static BOND: string;
        static OPTION: string;
        static FUTURE_FORWARD: string;
        static CERTIFICATE: string;
        static WARRANT: string;
        static ETF: string;
        static INDEX: string;
        static PREMIUM_BOND: string;
        static SUBSCRIPTION_OPTION: string;
        static EQUITY_LINKED_BOND: string;
        static CONVERTIBLE: string;
        static TODAY: string;
        static ONE_MONTH: string;
        static THREE_MONTHS: string;
        static ONE_WEEK: string;
        static THIS_YEAR: string;
        static ONE_YEAR: string;
        static FIVE_YEARS: string;
        static HIGHEST_RATED_FUNDS: string;
        static LOWEST_FEE_INDEX_FUNDS: string;
        static BEST_DEVELOPMENT_FUNDS_LAST_THREE_MONTHS: string;
        static MOST_OWNED_FUNDS: string;
        static OPTIONS: string;
        static FOREX: string;
        static DEPOSIT_WITHDRAW: string;
        static BUY_SELL: string;
        static DIVIDEND: string;
        static INTEREST: string;
        static FOREIGN_TAX: string;
        static ACCOUNTS: string;
        static QUOTES: string;
        static ORDERDEPTHS: string;
        static TRADES: string;
        static BROKERTRADESUMMARY: string;
        static POSITIONS: string;
        static ORDERS: string;
        static DEALS: string;
        static BUY: string;
        static SELL: string;
    }

}
